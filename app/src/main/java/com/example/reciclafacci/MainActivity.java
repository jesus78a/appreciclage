package com.example.reciclafacci;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    Button button2;
    Button buttonlogin;
    EditText txtcorreo, txtcontraceña;
    TextInputLayout impcorreo, impcontraceña;

    boolean Cor=false,pas=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtcorreo = (EditText)findViewById(R.id.textCorreo);
        txtcontraceña = (EditText)findViewById(R.id.txtContaseña);

        impcorreo = (TextInputLayout)findViewById(R.id.impcorreo);
        impcontraceña = (TextInputLayout)findViewById(R.id.impcontaceña);

        buttonlogin = (Button)findViewById(R.id.buttonLogin);
        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Patterns.EMAIL_ADDRESS.matcher(txtcorreo.getText().toString()).matches()==false){
                    impcorreo.setError("correo invalido");
                    Cor=false;
                }else {
                    Cor=true;
                    impcorreo.setError(null);
                }

                Pattern p=Pattern.compile("[0-9][0-9][0-9][0-9][0-9]");
                if (p.matcher(txtcontraceña.getText().toString()).matches()==false){
                    impcontraceña.setError("contraseña invalida");
                    pas=false;
                }else {
                    pas=true;
                    impcontraceña.setError(null);
                }

                if(Cor==true && pas==true){
                    String usu = txtcorreo.getText().toString();
                    String clave = txtcontraceña.getText().toString();
                    if (usu.equals("jesus@facci.com") && clave.equals("12345")){
                        Intent i= new Intent(getApplicationContext(),Main3Activity.class);
                        startActivity(i);
                    }else{
                        Toast.makeText(getApplicationContext(), "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        button2 = findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);



            }
        });


    }

}

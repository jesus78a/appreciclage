package com.example.reciclafacci;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HitorialActivity extends AppCompatActivity {
    TextView Nombre;
    TextView Telefono;
    TextView Correo1;
    TextView Descrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hitorial);
        Nombre = (TextView) findViewById(R.id.recibirNombre);
        Telefono = (TextView) findViewById(R.id.recibirTelefono);
        Correo1 =findViewById(R.id.recibirCorreoo);
        Descrip = findViewById(R.id.recibirDescripcion);

        Bundle bundle =this.getIntent().getExtras();

        Nombre.setText(bundle.getString("dato"));
        Telefono.setText(bundle.getString("dato2"));
        Correo1.setText(bundle.getString("dato3"));
        Descrip.setText(bundle.getString("dato4"));
    }
}

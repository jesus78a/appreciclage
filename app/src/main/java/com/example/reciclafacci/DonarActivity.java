package com.example.reciclafacci;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DonarActivity extends AppCompatActivity
      implements View.OnClickListener
{

        // Request code
        private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
        private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;
        //Valor de la constante para la camara
        static final int REQUEST_IMAGE_CAPTURE = 1;
        //Instancia de Bitmap para la creacion de la imagen
        Bitmap imageBitmap;
        //Constantes para la gestión de archivos y carpetas
        private final String FOLDER_NAME = "UOCImageApp";
        private final String FILE_NAME = "imageapp.jpg";

        // Views
        private Button buttonOpenImage;
        private ImageView imageView;
        Button bottonDonar;
        EditText donacion;
        EditText telefono;
        EditText correo;
        EditText descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donar);
        // Set views
        buttonOpenImage = findViewById(R.id.buttonCapture);
        imageView = findViewById(R.id.imagecapture);
        imageView = findViewById(R.id.imagecapture);
        donacion = findViewById(R.id.edittextDonar);
        telefono = findViewById(R.id.buttonTelefono);
        correo = findViewById(R.id.donar_Correo);
        descripcion = findViewById(R.id.donarDescripcion);

        bottonDonar = (Button) findViewById(R.id.ButtonDonar);

        // Set listeners
        buttonOpenImage.setOnClickListener(this);
        bottonDonar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DonarActivity.this, HitorialActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",donacion.getText().toString());
                bundle.putString("dato2",telefono.getText().toString());
                bundle.putString("dato3",correo.getText().toString());
                bundle.putString("dato4",descripcion.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        //Define la pantalla de inicio dependiendo si existe o no ya una
        //imagen almacenada en la SDCard
        cargarImagenSiExiste();
    }


    private void onDeleteMenuTap() {
        // check permissions
        if (!hasPermissionsToWrite()) {
            // request permissions
            ActivityCompat.requestPermissions(
                    this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_DELETE);
        } else {
            //Ventana de diálogo para preguntarle al usuario si está seguro
            //de querer eliminar la imagen
            DialogInterface.OnClickListener dialogClickListener =
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked
                                    try {
                                        deleteImageFile();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };
            //Diseñamos la pantalla con la pregunta y las opciones
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Eliminar Imagen")
                    .setMessage("Desea eliminar esta imagen?")
                    .setPositiveButton("Si", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener)
                    .show();
        }
    }

    private boolean hasPermissionsToWrite() {
        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void onSaveMenuTap() {
        // check permissions
        if (!hasPermissionsToWrite()) {
            // request permissions
            ActivityCompat.requestPermissions(
                    this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_SAVE);
        } else {

            if (imageView.getDrawable() != null) {
                //Create a folder named UOCImageApp
                createFolder();
                //save Directory
                String storageDir =
                        Environment.getExternalStorageDirectory() + "/UOCImageApp/";
                //Save the photo in the specified dir
                createImageFile(storageDir, this.FILE_NAME, imageBitmap);
            } else {
                Toast.makeText(this, "Tome una foto primero!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    String mCurrentPhotoPath;

    private void createImageFile(
            String storageDir, String fileName, Bitmap bitmap) {

        try {

            File myFile = new File(storageDir, fileName);
            FileOutputStream stream = new FileOutputStream(myFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
            Toast.makeText(this,
                    "Imagen guardada!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void deleteImageFile() throws IOException {

        File storageDir = new File(
                Environment.getExternalStorageDirectory() + "/UOCImageApp");
        File image = new File(storageDir + "/" + this.FILE_NAME);

        //Si existe el archivo entonces lo elimina, caso contrario
        //envía un mensaje de que no se encuentra el archivo
        if (image.exists()) {
            image.delete();
            Toast.makeText(this, "File deleted!", Toast.LENGTH_LONG).show();
            imageView.setImageResource(0);
        }
        else {
            Toast.makeText(this, "File not found!", Toast.LENGTH_LONG).show();

        }
    }


    //Procedimiento que carga la imagen desde la SDCard si es que existe, caso contrario
    // muestra el elemento TextView con el mensaje
    private void cargarImagenSiExiste()
    {
        //OBTENEMOS LA DIRECCION DE LA IMAGEN
        String myImage = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/" + FOLDER_NAME + "/" + FILE_NAME;
        //OBTENEMOS LA IMAGEN
        File imagen =new File(myImage);
        if(imagen.exists()){
            //LEER LA IMAGEN DE LA SDCARD UTILIZANDO PICASSO LIBRAR
        }
    }

    //Procedimiento que se encarga de la creación de la carpeta con el nombre UOCImageApp
    private void createFolder(){

        String myfolder = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/"+ FOLDER_NAME;

        File folder =new File(myfolder);
        if(!folder.exists())
            if(!folder.mkdir()){
                Toast.makeText(this,
                        FOLDER_NAME + " no se puede crear ",
                        Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(this,
                        FOLDER_NAME + " creada exitosamente",
                        Toast.LENGTH_LONG).show();
    }



    @Override
    public void onClick(View v) {
        if (v == buttonOpenImage) {
            TakePictureIntent();
        }
    }

    private void TakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //Despues que la activity de la cámara toma la foto y el usuario acepta la foto
    //esta se asigna al ImageView para ser mostrado, tambien se oculta el TextView
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //Obtenemos de data la imagen de la cámara
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            //Asiganmos la iamgen al ImageView
            imageView.setImageBitmap(imageBitmap);
            //Ocultamos el TextView
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_STORAGE_DELETE: {
                if (grantResults.length > 0 && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this,
                            "Permiso de eliminación denegado", Toast.LENGTH_LONG).show();
                }
            }
            case REQUEST_PERMISSION_STORAGE_SAVE: {
                if (grantResults.length > 0 && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    createFolder();
                    String storageDir =
                            Environment.getExternalStorageDirectory()+"/UOCImageApp/";
                    createImageFile(storageDir, FILE_NAME, imageBitmap);
                } else {
                    Toast.makeText(this,
                            "Permiso de guardar denegado", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
